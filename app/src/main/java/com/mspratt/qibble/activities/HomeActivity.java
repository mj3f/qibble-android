package com.mspratt.qibble.activities;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.mspratt.qibble.AppConstants;
import com.mspratt.qibble.R;
import com.mspratt.qibble.fragments.FeedFragment;
import com.mspratt.qibble.fragments.HomeActivityFragment;
import com.mspratt.qibble.fragments.ProfileFragment;
import com.mspratt.qibble.models.User;
import com.mspratt.qibble.services.InterceptorType;
import com.mspratt.qibble.services.interfaces.UserService;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class HomeActivity extends AppCompatActivity {

    private UserService mUserService;
    private ProfileFragment mProfileFragment;
    private FeedFragment mFeedFragment;
    private Toolbar mToolbar;

    private String mFeedFragmentTag = "returnToFeed";

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.nav_feed:
                    changeFragmentView(mFeedFragment);
                    return true;
                case R.id.nav_add_button:
                    return true;
                case R.id.nav_profile:
                    if (mProfileFragment == null) {
                        mProfileFragment = new ProfileFragment();
                    }
                    changeFragmentView(mProfileFragment);
                    return true;
                default:
                    break;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        this.mUserService = AppConstants.serviceGenerator.createService(UserService.class, InterceptorType.AUTH);
        setupTabBar();
        mFeedFragment = new FeedFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.layout_fragmentContainer, mFeedFragment);
        transaction.addToBackStack(null);
        transaction.commit();

        mToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(mToolbar);

//        mUserService.getById("1")
//                .subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Consumer<User>() {
//                    @Override
//                    public void accept(User user) throws Exception {
//                        Log.d("d", user.username);
//                    }
//                }, new Consumer<Throwable>() {
//                    @Override
//                    public void accept(Throwable throwable) throws Exception {
//                        Log.d("DEBUG", "Error");
//                    }
//                }, new Action() {
//                    @Override
//                    public void run() throws Exception {
//
//                    }
//                });
        // TODO: Design UI.
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.toolbar_menu_button:
                // edit button pressed
                Intent editProfileSegue = new Intent(HomeActivity.this, EditProfileActivity.class);
                startActivity(editProfileSegue);
                return true;
            default:
                return false;
        }
    }

    private void setupTabBar() {
        BottomNavigationView tabBar = findViewById(R.id.home_bottom_navigation);
        tabBar.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    private void changeFragmentView(final HomeActivityFragment fragment) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.layout_fragmentContainer, fragment, mFeedFragmentTag);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
    }

    @Override
    public void onBackPressed() {
        final HomeActivityFragment feedFragment = (HomeActivityFragment)
                getSupportFragmentManager().findFragmentByTag(mFeedFragmentTag);
        if (feedFragment.allowBackPressed()) {
            super.onBackPressed();
        }
    }
}
