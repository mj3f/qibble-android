package com.mspratt.qibble.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.mspratt.qibble.AppConstants;
import com.mspratt.qibble.R;
import com.mspratt.qibble.ioc.DaggerServicesComponent;
import com.mspratt.qibble.ioc.ServicesComponent;
import com.mspratt.qibble.ioc.ServicesProviderModule;
import com.mspratt.qibble.models.DTO.LoginDTO;
import com.mspratt.qibble.services.InterceptorType;
import com.mspratt.qibble.services.interfaces.AuthService;
import com.mspratt.qibble.settings.UserSettings;
import com.q42.qlassified.Qlassified;
import com.q42.qlassified.Storage.QlassifiedSharedPreferencesService;

import dagger.android.DaggerService;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class LoginActivity extends AppCompatActivity {

    private AuthService mAuthService;
    private UserSettings mUserSettings;

    private EditText mUsernameEditText;
    private EditText mPasswordEditText;
    private Switch mSaveCredentialsSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        AppConstants.serviceGenerator = DaggerServicesComponent.builder().servicesProviderModule(new ServicesProviderModule(AppConstants.PRODUCTION_API)).build().makeService();
        this.mAuthService = AppConstants.serviceGenerator.createService(AuthService.class, InterceptorType.STANDARD);


        mUsernameEditText = findViewById(R.id.loginActivity_editText_username);
        mPasswordEditText =  findViewById(R.id.loginActivity_editText_password);
        mSaveCredentialsSwitch = findViewById(R.id.switch_saveCredentials);

        Qlassified.Service.start(this);
        Qlassified.Service.setStorageService(new QlassifiedSharedPreferencesService(this, AppConstants.applicationIdentifier));

        this.mUserSettings = getUserSettings();
        setSavedCredentials(this.mUserSettings);
    }

    public void loginButtonClicked(View view) {
        hideKeyboard();

        // Check if credentials are valid, proceed to next activity if they are, otherwise notify the user that the creds failed.

        if (mUsernameEditText == null || mPasswordEditText == null) {
            return;
        }
        String username = mUsernameEditText.getText().toString();
        String password = mPasswordEditText.getText().toString();
        LoginDTO loginDTO = new LoginDTO(username, password);
        final Intent homeActivityIntent = new Intent(LoginActivity.this, HomeActivity.class);
        startActivity(homeActivityIntent);
        // authenticateUser(loginDTO);
    }

    private void hideKeyboard() {
        try {
            InputMethodManager inputManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);

            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("CheckResult")
    private void authenticateUser(LoginDTO loginDTO) {
        this.mAuthService.login(loginDTO)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String authToken) throws Exception {
                        if (authToken != null) {
                            if (mSaveCredentialsSwitch.isChecked()) {
                                saveCredentials();
                            }
                            AppConstants.serviceGenerator.getHeaders(InterceptorType.STANDARD).addHeader("Authorization", "Bearer " + authToken);
                            final Intent homeActivityIntent = new Intent(LoginActivity.this, HomeActivity.class);
                            startActivity(homeActivityIntent);
                        } else {
                            loginFailed();
                        }
                        Log.d("DEBUG", authToken);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.d("DEBUG", "Error");
                        loginFailed();
                    }
                }, new Action() {
                    @Override
                    public void run() throws Exception {

                    }
                });
    }

    private void loginFailed() {
        // TODO: Convert to dialog fragment (generic)
        Toast.makeText(this, "Username or password was incorrect.", Toast.LENGTH_SHORT).show();
    }

    private void setSavedCredentials(UserSettings userSettings) {

        mUsernameEditText.setText(userSettings.getUsername());
        mPasswordEditText.setText(userSettings.getPassword());
        mSaveCredentialsSwitch.setChecked(mUserSettings.isSaveUserCredentials());
    }

    private void saveCredentials() {
        final Switch saveCredentialsSwitch = findViewById(R.id.switch_saveCredentials);

        if (saveCredentialsSwitch.isChecked()) {

            if (mUsernameEditText == null || mPasswordEditText == null) {
                return;
            }
            this.mUserSettings.setUsername(mUsernameEditText.getText().toString());
            this.mUserSettings.setPassword(mPasswordEditText.getText().toString());

        } else {
            this.mUserSettings.setPassword("");
            this.mUserSettings.setUsername("");
        }

        this.mUserSettings.setSaveUserCredentials(saveCredentialsSwitch.isChecked());

        Qlassified.Service.put(AppConstants.saveUsername,this.mUserSettings.getUsername());
        Qlassified.Service.put(AppConstants.savePass,this.mUserSettings.getPassword());
        Qlassified.Service.put(AppConstants.saveCred, this.mUserSettings.isSaveUserCredentials());
    }

    private UserSettings getUserSettings() {

        UserSettings settings = new UserSettings();

        try {

            String username = Qlassified.Service.getString(AppConstants.saveUsername);
            String password = Qlassified.Service.getString(AppConstants.savePass);
            Boolean saveCred = Qlassified.Service.getBoolean(AppConstants.saveCred);

            settings.setUsername(username);
            settings.setPassword(password);
            settings.setSaveUserCredentials(saveCred);

        } catch (Exception ex) {
            Log.d("DEBUG" , ex.getMessage());
        }

        return settings;
    }
}
