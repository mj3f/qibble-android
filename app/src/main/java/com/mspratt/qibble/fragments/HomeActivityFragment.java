package com.mspratt.qibble.fragments;

import android.support.v4.app.Fragment;

public abstract class HomeActivityFragment extends Fragment {
    public abstract boolean allowBackPressed();
}
