package com.mspratt.qibble.ioc;

import com.mspratt.qibble.services.interfaces.ServiceGenerator;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ServicesProviderModule.class})
public interface ServicesComponent {
    ServiceGenerator makeService();
}
