package com.mspratt.qibble.ioc;

import com.mspratt.qibble.services.RetrofitAuthInterceptor;
import com.mspratt.qibble.services.RetrofitInterceptor;
import com.mspratt.qibble.services.RetrofitJwtDecoder;
import com.mspratt.qibble.services.RetrofitServiceGenerator;
import com.mspratt.qibble.services.RetrofitServiceHeaders;
import com.mspratt.qibble.services.interfaces.AuthHeaderInterceptor;
import com.mspratt.qibble.services.interfaces.HeaderInterceptor;
import com.mspratt.qibble.services.interfaces.JWTDecode;
import com.mspratt.qibble.services.interfaces.ServiceGenerator;
import com.mspratt.qibble.services.interfaces.ServiceHeaders;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ServicesProviderModule {

    private String API_BASE_URL;

    public ServicesProviderModule(String apiUrl) {
        API_BASE_URL = apiUrl;
    }

    @Provides
    @Singleton
    JWTDecode providesJwtDecode() {
        return new RetrofitJwtDecoder();
    }

    @Provides
    @Singleton
    ServiceHeaders provideServiceHeaders() {
        return new RetrofitServiceHeaders();
    }

    @Provides
    @Singleton
    AuthHeaderInterceptor providesAuthHeaderInterceptor(ServiceHeaders serviceHeaders, JWTDecode jwtDecode) {
        return new RetrofitAuthInterceptor(serviceHeaders, jwtDecode);
    }

    @Provides
    @Singleton
    HeaderInterceptor providesHeaderInterceptor(ServiceHeaders serviceHeaders) {
        return new RetrofitInterceptor(serviceHeaders);
    }


    @Provides
    @Singleton
    ServiceGenerator providesServiceGenerator(HeaderInterceptor headerInterceptor, AuthHeaderInterceptor authHeaderInterceptor) {
        return new RetrofitServiceGenerator(headerInterceptor, authHeaderInterceptor, API_BASE_URL);
    }

}
