package com.mspratt.qibble.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public abstract class BaseModel {

    @SerializedName("id")
    @Expose
    public Integer id;
}
