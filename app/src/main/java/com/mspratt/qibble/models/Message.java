package com.mspratt.qibble.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Message extends BaseModel {

    @SerializedName("content")
    @Expose
    public String content;

    @SerializedName("longitude")
    @Expose
    public Double longitude;

    @SerializedName("latitude")
    @Expose
    public Double latitude;

    @SerializedName("user_id")
    @Expose
    public Integer userId;
}
