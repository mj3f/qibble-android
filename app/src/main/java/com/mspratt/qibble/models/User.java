package com.mspratt.qibble.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class User extends BaseModel {

    @SerializedName("username")
    @Expose
    public String username;

    @SerializedName("password")
    @Expose
    public String password;

    @SerializedName("mobile_devices")
    @Expose
    public List<MobileDevice> mobileDevices;

    @SerializedName("messages")
    @Expose
    public List<Message> messages;
}
