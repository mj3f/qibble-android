package com.mspratt.qibble.services;

public enum InterceptorType {
    NONE,
    STANDARD,
    AUTH,
}
