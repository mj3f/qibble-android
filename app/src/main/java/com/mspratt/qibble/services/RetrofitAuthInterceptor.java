package com.mspratt.qibble.services;

import android.util.Log;

import com.mspratt.qibble.services.interfaces.AuthHeaderInterceptor;
import com.mspratt.qibble.services.interfaces.AuthService;
import com.mspratt.qibble.services.interfaces.JWTDecode;
import com.mspratt.qibble.services.interfaces.ServiceHeaders;

import java.io.IOException;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import okhttp3.Request;
import okhttp3.Response;

public class RetrofitAuthInterceptor implements AuthHeaderInterceptor {

    private static final String AUTH_TOKEN_KEY = "Authorization";
    private ServiceHeaders headers;
    private JWTDecode decoder;
    private AuthService authService;

    @Inject
    public RetrofitAuthInterceptor(ServiceHeaders headers, JWTDecode decoder) {
        this.headers = headers;
        this.decoder = decoder;
    }

    @Override
    public void addAuth(AuthService authService) {
        this.authService = authService;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {

        final Request.Builder builder = chain.request().newBuilder();

        System.out.println("Intercepting Auth");
        System.out.println(this.authService != null ? "Auth Service available" : "No Auth Service");

        if (this.headers.getHeaderMap().containsKey(AUTH_TOKEN_KEY)) {
            if (this.decoder.getDecodedWebToken(this.headers.getHeaderMap().get(AUTH_TOKEN_KEY).replace("Bearer" , "")).isExpired(60)) {
                System.out.println("Refresh Required");
                // Assumed headers already contain Auth Token
                if (this.authService != null) {
                    System.out.println("Auth Service - Available");
//                    this.authService.refresh().subscribe(new Consumer<Auth>() {
//                        @Override
//                        public void accept(Auth auth) throws Exception {
//                            System.out.println("Replace Auth Token");
//                            headers.getHeaderMap().put(AUTH_TOKEN_KEY, "Bearer " + auth.token);
//                            updateHeaders(builder);
//                        }
//                    }, new Consumer<Throwable>() {
//                        @Override
//                        public void accept(Throwable throwable) throws Exception {
//                            Log.e("Error", "Error occured with Auth Service in RetroFitAuthInterceptor! HTTP 401 Unauthorised.");
//                            // throwable.printStackTrace();
//                        }
//                    }, new Action() {
//                        @Override
//                        public void run() throws Exception {
//
//                        }
//                    });
                }
            } else {
                System.out.println("Not expired");
                updateHeaders(builder);
            }
        } else {
            updateHeaders(builder);
        }

        return chain.proceed(builder.build());
    }

    @Override
    public ServiceHeaders getHeaders() {
        return this.headers;
    }

    private void updateHeaders(Request.Builder builder) {

        if (this.headers != null && this.headers.getHeaderMap() != null) {
            Map<String, String> map = this.headers.getHeaderMap();
            for (Map.Entry<String, String> entry : map.entrySet()) {
                System.out.println("Update Headers " + entry.getKey() + " " + entry.getValue());
                builder.header(entry.getKey(), entry.getValue());
            }
        }
    }
}