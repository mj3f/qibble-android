package com.mspratt.qibble.services;

import com.auth0.android.jwt.JWT;
import com.mspratt.qibble.services.interfaces.JWTDecode;

import javax.inject.Inject;

public class RetrofitJwtDecoder implements JWTDecode {

    @Inject
    public RetrofitJwtDecoder() {
    }

    @Override
    public JWT getDecodedWebToken(String token) {

        return new JWT(token);
    }

}
