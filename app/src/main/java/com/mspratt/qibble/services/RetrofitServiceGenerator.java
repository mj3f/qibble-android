package com.mspratt.qibble.services;

import com.mspratt.qibble.services.InterceptorType;
import com.mspratt.qibble.services.interfaces.AuthHeaderInterceptor;
import com.mspratt.qibble.services.interfaces.AuthService;
import com.mspratt.qibble.services.interfaces.HeaderInterceptor;
import com.mspratt.qibble.services.interfaces.ServiceGenerator;
import com.mspratt.qibble.services.interfaces.ServiceHeaders;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitServiceGenerator implements ServiceGenerator {

    private String API_URL = "";
    private OkHttpClient.Builder httpClientBuilder;
    private Retrofit.Builder retrofitBuilder;
    private Retrofit retrofit;

    Map<InterceptorType, HeaderInterceptor> interceptors = new HashMap<>();

    @Inject
    public RetrofitServiceGenerator(HeaderInterceptor interceptor, AuthHeaderInterceptor authHeaderInterceptor, String apiBase) {

        API_URL = apiBase;
        OkHttpClient httpClient = new OkHttpClient();
        Retrofit.Builder builder = new Retrofit.Builder();

        this.httpClientBuilder = httpClient.newBuilder();
        this.httpClientBuilder.connectTimeout(60, TimeUnit.SECONDS);
        this.httpClientBuilder.readTimeout(60, TimeUnit.SECONDS);
        this.httpClientBuilder.writeTimeout(60, TimeUnit.SECONDS);
        this.retrofitBuilder = builder;

        this.retrofit = this.retrofitBuilder.baseUrl(API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                 .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        interceptors.put(InterceptorType.STANDARD, interceptor);
        authHeaderInterceptor.addAuth(createService(AuthService.class, InterceptorType.STANDARD));

        interceptors.put(InterceptorType.AUTH, authHeaderInterceptor);
    }

    @Override
    public ServiceHeaders getHeaders(InterceptorType type) {
        return interceptors.get(type).getHeaders();
    }

    @Override
    public <S> S createService(Class<S> service, InterceptorType type) {
        // intercepts the HTTP request to see if the Authorization header is needed to be added to the request before it is sent to the server for a response.
        // If type is AUTH, then an authorization header is needed.
        HeaderInterceptor headerInterceptor = interceptors.get(type);

        if (!httpClientBuilder.interceptors().contains(headerInterceptor)) {
            httpClientBuilder.addInterceptor(headerInterceptor);
            this.retrofitBuilder = this.retrofitBuilder.client(httpClientBuilder.build()); // Build new HttpClient (which includes the interceptors).
            this.retrofit = this.retrofitBuilder.build(); // Build new instance of Retrofit.
        }

        return retrofit.create(service);
    }
}
