package com.mspratt.qibble.services.interfaces;

public interface AuthHeaderInterceptor extends HeaderInterceptor {
    void addAuth(AuthService service);
}