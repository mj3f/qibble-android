package com.mspratt.qibble.services.interfaces;

import okhttp3.Interceptor;

public interface HeaderInterceptor extends Interceptor {

    ServiceHeaders getHeaders();
}