package com.mspratt.qibble.services.interfaces;

import com.mspratt.qibble.models.MobileDevice;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface MobileDeviceService {

    @POST("mobile-device/")
    Observable<String> createMobileDevice(@Body MobileDevice mobileDevice);

    @PUT("mobile-device/{id}")
    Observable<String> updateMobileDevice(@Path("id") String id, @Body MobileDevice mobileDevice);

    @DELETE("mobile-device/{id}")
    Observable<String> deleteMobileDevice(@Path("id") String id);
}
