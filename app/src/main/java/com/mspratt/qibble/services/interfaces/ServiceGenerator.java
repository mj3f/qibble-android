package com.mspratt.qibble.services.interfaces;

import com.mspratt.qibble.services.InterceptorType;

public interface ServiceGenerator {
        ServiceHeaders getHeaders(InterceptorType type);
        <S> S createService(Class<S> service, InterceptorType type);
}
