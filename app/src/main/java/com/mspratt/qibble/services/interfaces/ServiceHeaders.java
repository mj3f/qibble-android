package com.mspratt.qibble.services.interfaces;

import java.util.Map;

public interface ServiceHeaders {

    void addHeader(String key, String value);

    Map<String, String> getHeaderMap();
}
