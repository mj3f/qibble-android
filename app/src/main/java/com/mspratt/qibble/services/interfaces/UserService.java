package com.mspratt.qibble.services.interfaces;

import com.mspratt.qibble.models.Message;
import com.mspratt.qibble.models.MobileDevice;
import com.mspratt.qibble.models.User;

import java.util.List;
import io.reactivex.Observable;

import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface UserService {

    @GET("user/")
    Observable<List<User>> getAll();

    @GET("user/{id}")
    Observable<User> getById(@Path("id") String id);

    @GET("user/{id}/mobile-devices")
    Observable<List<MobileDevice>> getUserMobileDevices(@Path("id") String id);

    @GET("user/{userId}/mobile-devices/{deviceId}")
    Observable<MobileDevice> getUserMobileDeviceById(@Path("userId") String userId, @Path("deviceId") String deviceId);

    @GET("user/{id}/messages")
    Observable<List<Message>> getUserMessages(@Path("id") String id);

    @POST("user/")
    Observable<String> createUser(@Body User user);

    @PUT("user/{id}")
    Observable<String> updateUser(@Path("id") String id, @Body User user);

    @DELETE("user/{id}")
    Observable<String> deleteUser(@Path("id") String id);
}
