package com.mspratt.qibble.settings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserSettings {

    @Expose
    @SerializedName("save_user_credentials")
    private boolean saveUserCredentials = false;

    @Expose
    @SerializedName("sign_in_offline")
    private boolean signInOffline = false;

    @Expose
    @SerializedName("username")
    private String username = "";

    @Expose
    @SerializedName("password")
    private String password = "";


    public boolean isSaveUserCredentials() {
        return saveUserCredentials;
    }

    public void setSaveUserCredentials(boolean saveUserCredentials) {
        this.saveUserCredentials = saveUserCredentials;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
